package com.kiki.service;

import com.kiki.pojo.User;

/**
 * @author kiki
 * @version 1.0
 * @create 2020/8/7 13:09
 */
public interface UserService {

    User queryUserByName(String name);
}






