package com.kiki.service;

import com.kiki.mapper.UserMapper;
import com.kiki.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author kiki
 * @version 1.0
 * @create 2020/8/7 13:10
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public User queryUserByName(String name) {
        return userMapper.queryUserByName(name);
    }
}
