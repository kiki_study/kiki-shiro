package com.kiki.mapper;

import com.kiki.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author kiki
 * @version 1.0
 * @create 2020/8/7 11:53
 */
@Repository
@Mapper
public interface UserMapper {

    User queryUserByName(String name);
}










